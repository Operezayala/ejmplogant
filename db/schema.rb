# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150127125957) do

  create_table "gantt_links", force: :cascade do |t|
    t.integer  "project_id",      limit: 4
    t.integer  "targetable_id",   limit: 4
    t.string   "targetable_type", limit: 255
    t.integer  "sourceable_id",   limit: 4
    t.string   "sourceable_type", limit: 255
    t.string   "gtype",           limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "gantt_links", ["project_id"], name: "index_gantt_links_on_project_id", using: :btree
  add_index "gantt_links", ["sourceable_type", "sourceable_id"], name: "index_gantt_links_on_sourceable_type_and_sourceable_id", using: :btree
  add_index "gantt_links", ["targetable_type", "targetable_id"], name: "index_gantt_links_on_targetable_type_and_targetable_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tasks", force: :cascade do |t|
    t.integer  "project_id", limit: 4
    t.string   "name",       limit: 255
    t.date     "start_date"
    t.integer  "duration",   limit: 4
    t.float    "progress",   limit: 24
    t.integer  "sortorder",  limit: 4
    t.integer  "parent",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tasks", ["project_id"], name: "index_tasks_on_project_id", using: :btree

end
